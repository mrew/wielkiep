/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pl.com.pb.proxyserver;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Mehim
 */
public class Statistics {
    
    private static final Map<String,Map<String,Long>> domainStatistics;
    private static final Map<String,Long> globalStatistics;
    private static final Map<String,Map<String,Long>> globalEnumStatistics;
    
    static {
        domainStatistics = new HashMap<>();
        globalStatistics = new HashMap<>();
        globalEnumStatistics = new HashMap<>();
    }
    
    public static void incrementDomainValue(String domain, String key){
        Map<String,Long> pageStats;
        if(!domainStatistics.containsKey(domain)){
            pageStats = new HashMap<>();
            domainStatistics.put(domain, pageStats);
        } else {
            pageStats = domainStatistics.get(domain);
        }
        
        Long value;
        if(!pageStats.containsKey(key)){
            value = 0L;
            pageStats.put(key, value);
        } else {
            value = pageStats.get(key);
        }
        
        value++;
        pageStats.replace(key, value);
    }
    
    public static Long getDomainValue(String domain, String key){
        if(!domainStatistics.containsKey(domain) && !domainStatistics.get(domain).containsKey(key)){
            return 0L;
        }
        return domainStatistics.get(domain).get(key);
    }
    
    public static void setDomainValue(String domain, String key, Long value){
        Map<String,Long> pageStats;
        if(!domainStatistics.containsKey(domain)){
            pageStats = new HashMap<>();
            domainStatistics.put(domain, pageStats);
        } else {
            pageStats = domainStatistics.get(domain);
        }
        
        if(!pageStats.containsKey(key)){
            pageStats.put(key, value);
        } else {
            pageStats.replace(key, value);
        }
    }
    
    public static void incrementGlobalValue(String key){        
        Long value;
        if(!globalStatistics.containsKey(key)){
            value = 0L;
            globalStatistics.put(key, value);
        } else {
            value = globalStatistics.get(key);
        }
        
        value++;
        globalStatistics.replace(key, value);
    }
    
    public static Long getGlobalValue(String key){
        if(!globalStatistics.containsKey(key)){
            return 0L;
        }
        return globalStatistics.get(key);
    }
    
    public static void setGlobalValue(String key, Long value){
        if(!globalStatistics.containsKey(key)){
            globalStatistics.put(key, value);
        } else {
            globalStatistics.replace(key, value);
        }
    }
    
    public static void incrementGlobalEnumValue(String key, String enumKey){
        Map<String,Long> pageStats;
        if(!globalEnumStatistics.containsKey(key)){
            pageStats = new HashMap<>();
            globalEnumStatistics.put(key, pageStats);
        } else {
            pageStats = globalEnumStatistics.get(key);
        }
        
        Long value;
        if(!pageStats.containsKey(key)){
            value = 0L;
            pageStats.put(key, value);
        } else {
            value = pageStats.get(key);
        }
        
        value++;
        pageStats.replace(key, value);
    }
    
    public static Long getGlobalEnumValue(String key, String enumKey){
        if(!globalEnumStatistics.containsKey(key) && !globalEnumStatistics.get(key).containsKey(enumKey)){
            return 0L;
        }
        return globalEnumStatistics.get(key).get(enumKey);
    }
    
    public static void setGlobalEnumValue(String key, String enumKey, Long value){
        Map<String,Long> pageStats;
        if(!globalEnumStatistics.containsKey(key)){
            pageStats = new HashMap<>();
            globalEnumStatistics.put(key, pageStats);
        } else {
            pageStats = domainStatistics.get(key);
        }
        
        if(!pageStats.containsKey(enumKey)){
            pageStats.put(enumKey, value);
        } else {
            pageStats.replace(enumKey, value);
        }
    }
    
    public static void consume(Request request){
        //Most visited
        incrementGlobalEnumValue("VISITS_GLOBAL", request.getHost());
        
        //Biggest request
        Long size = getGlobalValue("LARGEST_REQUEST_SIZE");
        if(request.getDataSize() > size){
            setGlobalValue("LARGEST_REQUEST_SIZE", new Long(size));
        }
        
        //Browsers usage
        incrementGlobalEnumValue("BROWSER_USAGE", request.getBrowser().toString());
       
    }
    
}
