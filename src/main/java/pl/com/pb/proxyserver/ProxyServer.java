/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.com.pb.proxyserver;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.ServerSocket;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import sun.applet.Main;

public class ProxyServer {

    private static Logger LOGGER = Logger.getLogger(ProxyServer.class.getName());

    public static void main(String[] args) throws IOException {
        ServerSocket serverSocket = null;
        boolean listening = true;
        int port = 10000;

        final GuiBuilder guiBuilder = new GuiBuilder();
        guiBuilder.buildGUI();

        try {
            serverSocket = new ServerSocket(port);
            LOGGER.log(Level.INFO, "Listening on: " + port);
            while (listening) {
                new ProxyThread(serverSocket.accept()).start();
            }
            serverSocket.close();
        } catch (IOException e) {
            LOGGER.log(Level.SEVERE, null, e);
        }
    }
}
