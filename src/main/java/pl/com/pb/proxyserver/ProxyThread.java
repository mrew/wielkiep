package pl.com.pb.proxyserver;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.Socket;
import java.net.URL;
import java.net.URLConnection;
import java.util.Calendar;
import java.util.StringTokenizer;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Mehim
 */
public class ProxyThread extends Thread {
    
    private Socket socket = null;
    private static final int BUFFER_SIZE = 8192;
    private static Calendar calendar = Calendar.getInstance();
    
    public ProxyThread(Socket socket) {
        super("ProxyThread");
        this.socket = socket;
    }
    private static Logger LOGGER = Logger.getLogger(ProxyThread.class.getName());
    
    public void run() {

        try {
            DataOutputStream out =
		new DataOutputStream(socket.getOutputStream());
            BufferedReader in = new BufferedReader(
		new InputStreamReader(socket.getInputStream()));

            String inputLine;
            

            Request request = new Request();
            request.setTime(calendar.getTime());
            
            while ((inputLine = in.readLine()) != null) {
                System.out.println(inputLine);
                try {
                    StringTokenizer tok = new StringTokenizer(inputLine);
                    tok.nextToken();
                } catch (Exception e) {
                    break;
                }
                
                request.consumeInput(inputLine);

            }
            LOGGER.log(Level.INFO, "Request for : " + request.getURL());
                        
            BufferedReader rd = null;
            try {
                //Otwieramy polaczenie URL do wczytanego linka
                URL url = new URL(request.getURL());
                URLConnection conn = url.openConnection();
                conn.setDoInput(true);
                conn.setDoOutput(false);

                InputStream is = null;
                //HttpURLConnection huc = (HttpURLConnection)conn;
                
                if (conn.getContentLength() > 0) {
                    try {
                        is = conn.getInputStream();
                        rd = new BufferedReader(new InputStreamReader(is));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                

                byte by[] = new byte[ BUFFER_SIZE ];
                int index = is.read( by, 0, BUFFER_SIZE );

                while ( index != -1 )
                {
                  out.write( by, 0, index );
                  index = is.read( by, 0, BUFFER_SIZE );
                }
                request.setDataSize(new Long(out.size()));
                out.flush();

            } catch (Exception e) {
                LOGGER.log(Level.SEVERE, null, e);
                out.writeBytes("");
            }

            if (rd != null) {
                rd.close();
            }
            if (out != null) {
                out.close();
            }
            if (in != null) {
                in.close();
            }
            
            if (socket != null) {
                socket.close();
            }
            
            Statistics.consume(request);
            
        } catch (IOException e) {
            LOGGER.log(Level.SEVERE, null, e);
        } 
        
        
    }
}
