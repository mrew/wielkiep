/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.com.pb.proxyserver;

/**
 *
 * @author mrewu_000
 */
public class ProxyUtil {
    
    private static final String MSIE_USER_AGENT_VALUE = "MSIE";
    private static final String SAFARI_USER_AGENT_VALUE = "Safari";
    private static final String CHROME_USER_AGENT_VALUE = "Chrome";
    private static final String FIREFOX_USER_AGENT_VALUE = "Firefox";
    
    public static Browser getBrowserFromUserAgent(String userAgent){
        if(userAgent.contains(FIREFOX_USER_AGENT_VALUE)){
            return Browser.FIREFOX;
        } else if(userAgent.contains(CHROME_USER_AGENT_VALUE)){
            return Browser.CHROME;
        } else if(userAgent.contains(SAFARI_USER_AGENT_VALUE)){
            return Browser.SAFARI;
        } else if(userAgent.contains(MSIE_USER_AGENT_VALUE)){
            return Browser.IE;
        }
        return Browser.UNKNOWN;
    }

    
}
