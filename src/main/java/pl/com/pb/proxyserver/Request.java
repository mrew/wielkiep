/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pl.com.pb.proxyserver;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Mehim
 */
public class Request {
    
    private static final String CONNECTION = "Connection";
    private static final String REFERER = "Referer";
    private static final String USER_AGENT = "User-Agent";
    private static final String HOST = "Host";
    private static final String GET = "GET";
    private static final String POST = "POST";
    
    private static Logger LOGGER = Logger.getLogger(Request.class.getName());
    
    private String URL;
    private String referer;
    private String host;
    private Browser browser;
    private String connection;
    private Long dataSize;
    private Date time;
    
    public Request(){
        referer = "";
        URL = "";
        host = "";
        connection = "";
        browser = Browser.UNKNOWN;
    }
    
    public String getURL() {
        return URL;
    }

    public void setURL(String URL) {
        this.URL = URL;
    }

    public String getReferer() {
        return referer;
    }

    public void setReferer(String referer) {
        this.referer = referer;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public Long getDataSize() {
        return dataSize;
    }

    public void setDataSize(Long dataSize) {
        this.dataSize = dataSize;
    }

    public Browser getBrowser() {
        return browser;
    }

    public void setBrowser(Browser browser) {
        this.browser = browser;
    }

    public String getConnection() {
        return connection;
    }

    public void setConnection(String connection) {
        this.connection = connection;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public void consumeInput(String input){
        
        String[] tokens = input.split(" ");
        String attributeName = tokens[0];
        attributeName = attributeName.replace(":", "");
        String value = tokens[1];
        
        switch(attributeName){
            case GET:
            case POST:
                URL = value;
                break;
            case HOST:
                host = value;
                break;
            case USER_AGENT:
                browser = ProxyUtil.getBrowserFromUserAgent(input);
                break;
            case REFERER:
                 try {
                        URI uri = new URI(value);
                        String path = uri.getHost();
                        referer = path;
                    } catch (URISyntaxException ex) {
                        LOGGER.log(Level.SEVERE, null, ex);
                    }
                break;
            case CONNECTION:
                connection = value;
                break;
        }
    }

    @Override
    public String toString() {
        return "Request{" + "URL=" + URL + ", referer=" + referer + ", host=" + host + ", browser=" + browser + ", connection=" + connection + '}';
    }
}
